﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace UPP
{
    public partial class View : Form
    {
        Controller C = new Controller();
        string tableName = "";
        int row;
        int cell;
        int currentMyComboBoxIndex;

        public View()
        {
            InitializeComponent();
            dataGridView2.DataSource = C.getNameTables().Tables[0].DefaultView;
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)//показать
        {
            dataGridView1.DataSource = C.ShowTable(tableName).Tables[0].DefaultView;
            int c = dataGridView1.ColumnCount;
            //dataGridView1.Columns[0].ReadOnly = true;
            //dataGridView1.Columns[c - 3].ReadOnly = true;
            //dataGridView1.Columns[c - 2].ReadOnly = true;
            //dataGridView1.Columns[c - 1].ReadOnly = true;
        }

        private void button2_Click(object sender, EventArgs e)//добавить
        {
            int countColumn = dataGridView1.ColumnCount;
            C.add(countColumn, dataGridView1, tableName, row);
            dataGridView1.DataSource = C.ShowTable(tableName).Tables[0].DefaultView;
        }

        private void button3_Click(object sender, EventArgs e)//изменить
        {

            string id = dataGridView1.Rows[row].Cells[0].Value.ToString();
            string columnName = dataGridView1.Columns[cell].HeaderText.ToString();
            var value = dataGridView1.Rows[row].Cells[cell].Value;
            C.change(id, tableName, columnName, value);
            dataGridView1.DataSource = C.ShowTable(tableName).Tables[0].DefaultView;
        }

        private void button4_Click(object sender, EventArgs e)//удалить
        {
            string id = dataGridView1.Rows[row].Cells[0].Value.ToString();
            C.delete(id, tableName);
            dataGridView1.DataSource = C.ShowTable(tableName).Tables[0].DefaultView;
        }

        private void dataGridView2_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            int i = e.ColumnIndex;
            int j = e.RowIndex;
            var value = dataGridView2.Rows[j].Cells[i].Value;
            tableName = value.ToString();
            label1.Text = tableName;
            ComboBox(tableName);
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            row = e.RowIndex;
            label4.Text = row.ToString();
            cell = e.ColumnIndex;
            label3.Text = cell.ToString();
        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        public void ComboBox(string tableName)
        {
            if (tableName == "users")
            {
                comboBox1.Items.Clear();
                comboBox1.Items.Add("Доступный список врачей по id");
            }
            if (tableName == "history_b")
            {
                comboBox1.Items.Clear();
                comboBox1.Items.Add("История болезни по id");
            }
            if (tableName == "law_user")
            {
                comboBox1.Items.Clear();
                comboBox1.Items.Add("Права доступа по id");
            }
            if (tableName == "specialty_user")
            {
                comboBox1.Items.Clear();
                comboBox1.Items.Add("Специальность по id");
            }
          
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            currentMyComboBoxIndex = comboBox1.SelectedIndex;
            if (tableName == "users")
            {
                DataSet ds = C.getListOfDoctor(Convert.ToInt32(textBox1.Text));
                dataGridView1.DataSource = ds.Tables[0].DefaultView;
            }
            if (tableName == "history_b")
            {
                DataSet ds = C.getHistoryById(Convert.ToInt32(textBox1.Text));
                dataGridView1.DataSource = ds.Tables[0].DefaultView;
            }
            if (tableName == "law_user")
            {
                DataSet ds = C.getAccess_right(Convert.ToInt32(textBox1.Text));
                dataGridView1.DataSource = ds.Tables[0].DefaultView;
            }
            if (tableName == "specialty_user")
            {
                DataSet ds = C.getSpecialtyById(Convert.ToInt32(textBox1.Text));
                dataGridView1.DataSource = ds.Tables[0].DefaultView;
            }
        }
    }
}
