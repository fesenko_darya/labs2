﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UPP.daos;
using System.Data;

namespace UPP.services
{
    class UserService
    {
        UserDao _userDao;

        public UserService(UserDao userDao)
        {
            _userDao = userDao;
        }

        public DataSet getListOfDoctor(int user_id)
        {
            DataSet ds = new DataSet();
           
            ds = _userDao.getById(user_id);
            
            int value = Convert.ToInt32(ds.Tables[0].Rows[0].ItemArray[0]);
            if (value == 0)
            {
                throw new Exception("Записи с таким id  не существует");
            }
            else 
            {
                ds = _userDao.getListOfDoctor(user_id);
            }
            return ds;
        }
    }
}
