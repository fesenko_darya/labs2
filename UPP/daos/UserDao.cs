﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UPP.daos.db;
using System.Data;

namespace UPP.daos
{
    class UserDao: Dao<UserModel>
    {
       // string tableName;
        DataBaseConnection db;

        public UserDao(DataBaseConnection db):base(db,"users")
        {
            this.db = db;
           
        }

        public DataSet getListOfDoctor(int user_id)
        {

            string sql = String.Format("{0}{1}{2}{3}{4}{5}'", "select users.surname, ",
                        "specialty.title from specialty, users, specialty_user",
                        " where specialty.id > 3 and specialty_user.id_specialty = '1'",
                        " and specialty_user.id_user = users.id and ((users.gender = 'ж' and specialty.title <> 'Андролог')",
                        " or (users.gender = 'м' and specialty.title <> 'Гинеколог')) and users.id ='", user_id);
            DataSet ds = db.query(sql);

            return ds;
        }
    }
}
