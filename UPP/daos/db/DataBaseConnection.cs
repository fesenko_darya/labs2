﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using System.Data;

namespace UPP.daos.db
{
    public class DataBaseConnection
    {
        private string my_connect_db = "Server=127.0.0.1;Database=upp;Uid=root;Pwd=1234;";

        public DataSet query(string sql)
        {
            MySqlConnection my_connection = new MySqlConnection(my_connect_db);
            MySqlCommand cmd;
            my_connection.Open();
            try
            {
                cmd = my_connection.CreateCommand();

                cmd.CommandText = sql;
                MySqlDataAdapter adp = new MySqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                adp.Fill(ds);
                my_connection.Close();
                return ds;
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
