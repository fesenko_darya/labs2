﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UPP.daos.db;
using System.Data;

namespace UPP.daos
{
    class Dao<T>
    {
        protected string tableName;
        private DataBaseConnection db;

        protected Dao(DataBaseConnection db, string tableName)
        {
            this.tableName = tableName;
            this.db = db;
        }


        public DataSet getAll()
        {
            DataSet ds = new DataSet();
            string sql = String.Format("Select * from {0}", tableName);
            ds = db.query(sql);
            return ds;
        }

        public DataSet getById(int id)
        {
            DataSet ds = new DataSet();
            string sql = String.Format("Select * from {0} where id = {1}", tableName, id);
            ds = db.query(sql);
            return ds;
        }
    }
}
