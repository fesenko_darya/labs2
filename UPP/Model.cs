﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace UPP
{
    public class UserModel
    {
        public int id { get; set; }
        public string surname { get; set; }
        public string first_name { get; set; }
        public string last_name { get; set; }
        public string adres { get; set; }
        public char gender { get; set; }
        public DateTime birth_day { get; set; }
        public Model m = new Model();

        public DataSet getListOfDoctor(int user_id)
        {
     
            string sql = String.Format("{0}{1}{2}{3}{4}{5}'", "select users.surname, ",
                        "specialty.title from specialty, users, specialty_user",
                        " where specialty.id > 3 and specialty_user.id_specialty = '1'",
                        " and specialty_user.id_user = users.id and ((users.gender = 'ж' and specialty.title <> 'Андролог')",
                        " or (users.gender = 'м' and specialty.title <> 'Гинеколог')) and users.id ='", user_id);
            DataSet ds = m.ConnectionToBase(sql);

            return ds;
        }


    }

    public class DiagnosModel
    {
        public int id { get; set; }
        public string type_diagnos { get; set; }
        public string info { get; set; }
    }

    public class HistoryModel
    {
        public int id { get; set; }
        public int id_user { get; set; }
        public int id_diagnos { get; set; }
        public Model m = new Model();

        public DataSet getHistoryById(int user_id)
        {
            string sql = String.Format("select users.surname, diagnos.type_diagnos,diagnos.info from history_b inner join users on history_b.id_user = users.id inner join diagnos on history_b.id_diagnos = diagnos.id where users.id = {0}", user_id);
            DataSet ds = m.ConnectionToBase(sql);

            return ds;
        }
    }

    public class LawModel
    {
        public int id { get; set; }
        public string title { get; set; }
    }

    public class LawUserModel
    {
        public int id { get; set; }
        public int id_user { get; set; }
        public int id_law { get; set; }
        public Model m = new Model();

        public DataSet getAccess_right(int user_id)
        {
            string sql = String.Format("select users.surname, law.title from law_user inner join law on law_user.id_law = law.id inner join users on law_user.id_user = users.id where users.id = {0}", user_id);
            DataSet ds = m.ConnectionToBase(sql);

            return ds;
        }
    }

    public class SpecialtyModel
    {
        public int id { get; set; }
        public string title { get; set; }
    }

    public class SpecialtyUserModel
    {
        public int id { get; set; }
        public int id_specialty { get; set; }
        public int id_user { get; set; }
        public Model m = new Model();

        public DataSet getSpecialtyById(int user_id)
        {
            string sql = String.Format("{0}{1}{2}{3}{4}{5}", "select users.surname, specialty.title",
                                        " from specialty_user inner join users on ",
                                        "specialty_user.id_user = users.id ",
                                        "inner join specialty on specialty_user.id_specialty ",
                                        "= specialty.id where users.id = ", user_id);
            DataSet ds = m.ConnectionToBase(sql);

            return ds;
        }
    }

    public class Model
    {
        string my_connect_db = "Server=127.0.0.1;Database=upp;Uid=root;Pwd=1234;";

        public DataSet ConnectionToBase(string sql)
        {
            MySqlConnection my_connection = new MySqlConnection(my_connect_db);
            MySqlCommand cmd;
            my_connection.Open();
            try
            {
                cmd = my_connection.CreateCommand();

                cmd.CommandText = sql;
                MySqlDataAdapter adp = new MySqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                adp.Fill(ds);
                my_connection.Close();
                return ds;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public void delete(string tableName, string id)
        {
            string sql = String.Format("delete from {0} where id = '{1}'", tableName, id);
            DataSet ds = ConnectionToBase(sql);
        }

        public int maxId(string nameTable)
        {
            string sql = String.Format("select max(id) from {0}", nameTable);
            DataSet ds = ConnectionToBase(sql);
            int id = Convert.ToInt32(ds.Tables[0].Rows[0].ItemArray[0]);
            return id;
        }

        public void add(int countColumn, DataGridView dataGridView, string tableName, int row)
        {
            int id = maxId(tableName) + 1;
            dataGridView.Rows[row].Cells[0].Value = id;

            var current_row = dataGridView.Rows[row];

            string sql = String.Format("'{0}'", id.ToString());
            for (int i = 1; i < countColumn; i++)
            {
                string nameColumn = dataGridView.Columns[i].HeaderText.ToString();
                if (nameColumn == "surname" || nameColumn == "first_name" || nameColumn == "last_name")
                {
                    string a = current_row.Cells[i].Value.ToString();
                    string sym = "1234567890!@#$%^&*()+=;',.&^`~<>/{}[]";
                    foreach (var k in a)
                    {
                        foreach (var s in sym)
                        {
                            if (k == s)
                            {
                                Error e = new Error(nameColumn);
                                e.Show();
                                return;
                            }
                        }
                    }
                }

                sql = sql + ",";
                if (current_row.Cells[i].Value.GetType() == typeof(int))
                    sql = String.Format("{0}{1}", sql, current_row.Cells[i].Value.ToString());
                else if (current_row.Cells[i].Value.GetType() == typeof(bool))
                    sql = String.Format("{0}{1}", sql, current_row.Cells[i].Value.ToString());
                else if (current_row.Cells[i].Value.GetType() == typeof(DateTime))
                {
                    DateTime d = Convert.ToDateTime(current_row.Cells[i].Value);
                    sql = String.Format("{0}date('{1}')", sql, d.ToString("yyyy-MM-dd"));
                }
                else sql = sql + "'" + current_row.Cells[i].Value.ToString() + "'";

            }

            string tmp = String.Format("insert into {0} value ({1});", tableName, sql);
            DataSet ds = ConnectionToBase(tmp);
        }

        public DataSet ShowTable(string tableName)
        {
            string sql = String.Format("select * from {0}", tableName);
            DataSet ds1 = ConnectionToBase(sql);
            return ds1;
        }

        public DataSet ShowTableById(string tableName, string id)
        {
            string sql;

            sql = String.Format("select * from {0} where id = '{1}'", tableName, id);
            DataSet ds1 = ConnectionToBase(sql);
            return ds1;

        }

        public void change<T>(string id, string tableName, string columnName, T value)
        {
            string sql;
            DataSet ds;

            if (columnName == "surname" || columnName == "first_name" || columnName == "last_name")
            {
                string a = Convert.ToString(value);
                string sym = "1234567890!@#$%^&*()+=;',.&^`~<>/{}[]";
                foreach (var k in a)
                {
                    foreach (var s in sym)
                    {
                        if (k == s)
                        {
                            Error e = new Error(columnName);
                            e.Show();
                            return;
                        }
                    }
                }
            }

            sql = String.Format("update {0} set {1} = '{2}' where id = '{3}'", tableName, columnName, value, id);
            ds = ConnectionToBase(sql);
        }

        public bool ExistsRecord(string id, string tableName)
        {
            string sql = String.Format("select count(id) from {0} where id = '{1}'",tableName, id);
            DataSet ds = ConnectionToBase(sql);
            int count = Convert.ToInt32(ds.Tables[0].Rows[0].ItemArray[0]);
            if (count == 1) return true;
            return false;
        }
    }

}
