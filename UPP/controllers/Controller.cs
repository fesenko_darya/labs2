﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
using UPP.daos.db;
using UPP.services;
using UPP.daos;

namespace UPP
{
    class Controller
    {
        Model m = new Model();
        UserModel um = new UserModel();
        HistoryModel hm = new HistoryModel();
        LawUserModel lum = new LawUserModel();
        SpecialtyUserModel sum = new SpecialtyUserModel();
        DataBaseConnection db = new DataBaseConnection();
      
 

        public void delete(string id, string tableName)
        {
            if (m.ExistsRecord(id, tableName))
            {
                m.delete(tableName, id);
            }
            else
            {
                Error e = new Error("Записи с таким id  не существует");
                e.Show();
            }
              
        }

        public int maxId(string nameTable)
        {
            int id = m.maxId(nameTable);
            return id;
        }

        public void add(int countColumn, DataGridView dataGridView, string tableName, int row)
        {
            m.add(countColumn,dataGridView,tableName,row);
        }

        public DataSet ShowTable(string tableName)
        {
            DataSet ds1 = m.ShowTable(tableName);
            return ds1;
        }

        public DataSet ShowTableById(string tableName, string id)
        {
            DataSet ds = new DataSet();
            if (m.ExistsRecord(id, tableName))
            {
                ds = m.ShowTableById(tableName, id);
               
            }
            else
            {
                Error e = new Error("Записи с таким id  не существует");
                e.Show();
            }
            return null;
        }

        public DataSet getNameTables()
        {
            string sql = "show tables from upp";
            DataSet ds = db.query(sql);
            return ds;
        }

        public void change<T>(string id, string tableName, string columnName, T value)
        {
            if (m.ExistsRecord(id, tableName))
            {
                m.change(id, tableName, columnName, value);
            }
            else
            {
                Error e = new Error("Записи с таким id  не существует");
                e.Show();
            }
           
        }

        public DataSet getListOfDoctor(int id)
        {
            DataSet ds = new DataSet();

            try
            {
                UserDao ud = new UserDao(db);
                UserService us = new UserService(ud);
                ds = us.getListOfDoctor(id);
            }
            catch(Exception e)
            {
                Error error = new Error(e.Message);
                error.Show();
            }
            return ds;
        }

        public DataSet getHistoryById(int id)
        {
            DataSet ds = hm.getHistoryById(id);
            return ds;
        }

        public DataSet getAccess_right(int id)
        {
            DataSet ds = lum.getAccess_right(id);
            return ds;
        }

        public DataSet getSpecialtyById(int id)
        {
            DataSet ds = sum.getSpecialtyById(id);
            return ds;
        }

    }
}
