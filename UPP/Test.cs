﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UPP
{
    class Test
    {
        Model m = new Model();
        

        public void TestChange()
        {
            m.change("5","users","surname", "Fesenko");//ошибка, не верный id
            m.change("2", "users", "surnames", "Fesenko");//ошибка, не верное название колонки
            m.change("2", "users", "surname", "Fesenko");//ошибки нет
        }

        public void TestDelete()
        {
            m.delete("users","2");//ошибки нет
            m.delete("user", "2");//ошибка, не верное название таблицы
            m.delete("users", "5");//ошибка, нет такого id
        }

        public void TestShowTable()
        {
            m.ShowTable("law");// ошибки нет
            m.ShowTable("lawuyt");// ошибка, не верное название таблицы
        }
    }
}
